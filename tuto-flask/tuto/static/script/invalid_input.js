const urlInput = document.getElementById("url-input");
    
urlInput.addEventListener("input", function () {
    const urlValue = urlInput.value;
    // vérifier si la saisie contient des lettres
    const letterRegex = /[a-zA-Z]/;
    // Vérifier si la valeur contient des lettres
    if (letterRegex.test(urlValue)) {
        // Si des lettres sont détectées, ajoutez une classe pour afficher la bordure en rouge
        urlInput.classList.add("invalid-input");
    } else {
        // Pas de lettre donc on retire la class
        urlInput.classList.remove("invalid-input");
    }
})