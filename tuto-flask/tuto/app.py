import os.path
from flask import Flask
from flask_bootstrap import Bootstrap5
from flask_login import LoginManager

app = Flask(__name__)
app.config['BOOTSTRAP_SERVE8LOCAL']=True
bootstrap = Bootstrap5(app)
app.config['SECRET_KEY'] = "bcc090e2-26b2-4c16-84ab-e766cc644320"
login_manager = LoginManager(app)
login_manager.init_app(app)

def mkpath(path):
    return (os.path.normpath(os.path.join(os.path.dirname(__file__),path)))

from flask_sqlalchemy import SQLAlchemy
app.config['SQLALCHEMY_DATABASE_URI'] = (
    'sqlite:///'+mkpath('../myapp.db'))
db = SQLAlchemy(app)

login_manager.login_view = "login"