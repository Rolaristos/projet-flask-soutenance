# Projet Flask Web
_Vous trouverez à votre disposition le fichier installation_module.sh qui vous permet d'installer tous les packages nécessaires au lancement du projet flask_
## _Équipe_:
    Roland RAKOTOMALALA
    Rebson DAHOUEDE

## Présentation générale du site web **_Ma Biblio_**:

Le site que nous avons développé se nomme Ma biblio et donne, aux visiteurs, l'accès à une petite bibliothèque virtuelle, ce dernier peut consulter la liste de tous les livres et leurs détails (notamment un lien Amazon pour se procurer le livre en question). Le visiteur peut également utiliser la barre de recherche pour rechercher un livre ou encore un auteur. Lorsqu'il clique sur un auteur, l'utilisateur obtient l'accès à la liste de tous les livres écris par celui-ci. 

Comme vous l'aurez compris, le site web offre la possibilité de s'inscrire pour avoir son propre compte et accéder à des fonctionnalités supplémentaires. Ces fonctionnalités sont les suivantes:

- Se connecter/déconnecter
- Créer un nouveau utilisateur
- Ajouter son propre livre.
- Ajouter un livre.
- Ajouter un auteur.
- Modifier le nom d'un auteur.
- Consulter la liste de ses livres.
- Publier un commentaire sur un livre en particulier.
- Supprimer un commentaire sous un de nos propres livres (ou un que nous avons écris).
- Supprimer un livre publié à notre nom

## Fonctionnalités non implémentées:

À présent, voici une liste des fonctionnalités que nous n'avons pas Ajouter au site Ma Biblio:

- Ajouter une note à un livre.
- Attribuer une catégorie à un livre. 
- Associer une description à chaque livres
